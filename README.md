# MARC Thermometer Hardware

This repository contains the schematics, printed circuit layouts and
BOMs for the digital thermometer constructed by the Midland Amateur
Radio Club in the Spring of 2015.

---

![Schematic](Wired/ThermometerPub800.png)

---
